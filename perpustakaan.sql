-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 01, 2020 at 09:19 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perpustakaan`
--

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `buku_id` varchar(10) NOT NULL,
  `judul` text NOT NULL,
  `pengarang` varchar(33) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `detail` text NOT NULL,
  `cover` text NOT NULL,
  `links` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`buku_id`, `judul`, `pengarang`, `tahun`, `detail`, `cover`, `links`) VALUES
('136579088', 'The Book of Wisdoms', 'Knotanumber', '2016', 'From Solomon to corn flakes, parrotfish to push-up bras, these bite-size verses contain a bit of everything under the sun. Short, pithy and often irreverent, the Book of Wisdoms is the scripture of the everyday and the wit of the commonplace. The be...', '136579088+188477532-352-k530370.jpg', 'https://www.wattpad.com/737627543-the-book-of-wisdoms-wisdom-of-solomon'),
('185857422', 'Negative Energy', 'Knotanumber', '2013', 'When Matt Harmon\'s brain pattern is re-bodified seventy-one years in the future, he finds an inner peace at odds with the outer desolation of the landscape. To pay off his debt to society, he must help a sentient power company ward off a conspiracy...\r\n\r\n', '185857422+208602843-512-k51688.jpg', 'https://www.wattpad.com/821007343-negative-energy-re-bodification'),
('479804542', 'Haladras', 'mfarzn', '2015', 'A desert planet. A dangerous secret. When Skylar\'s enigmatic uncle warns him to stay away from the mysterious winged insects that have been sighted on other planets, he thinks little of it; no one has seen the insects on their own planet of Haladras...', '479804542+43499649-352-k700691.jpg', 'https://www.wattpad.com/143342368-haladras-one'),
('593170207', 'testing3', 'testing', '2212', 'dwdad', '593170207+177358837-352-k50209.jpg', 'testing'),
('627563467', 'Captive Thoughts', 'Knotanumber', '2016', 'You want to know who you are and where you came from. I\'ll keep this short. I\'m not going to tell you.', '627563467+177358837-352-k50209.jpg', 'https://www.wattpad.com/689810074-captive-thoughts-read-me-first'),
('740305874', 'Radioactive Evolution', 'BalRog', '2015', 'The world as humanity knew it was gone. In its place was a radioactive wasteland, scorched by nuclear furnaces. The third millenium passed unmarked and uncelebrated by those in the safezones. The rich took to the oceans, and to the skies, leaving ev...\r\n\r\n', '740305874+144467843-352-k122767.jpg', 'https://www.wattpad.com/558428601-radioactive-evolution-chapter-1-evolution-unlocked');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` varchar(10) NOT NULL,
  `user_level` varchar(10) NOT NULL,
  `user_password` text NOT NULL,
  `user_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_level`, `user_password`, `user_name`) VALUES
('4746770762', 'admin', 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`buku_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
