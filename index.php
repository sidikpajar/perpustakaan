<?php include('header.php')?>

  <div class="container-custom">
    <div class="card-custom">
      <div class="big-picture">
        <img width="100%" src="login/files-photo/new.png" alt="The Mighty Morg by knotanumber">
      </div>
     
    </div>
    <div class="card-custom">
      <form action="index.php" method="get">
        
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-9">
                <input 
                  type="text" 
                  class="search-input" 
                  name="cari"
                  placeholder="Harry Potter"
                  >
              </div>
              <div class="col-md-2">
                <input 
                  class="search-button" 
                  type="submit" 
                  value="Cari judul buku"
                  >
              </div>
            </div>
            <div class="row">
              <?php 
                if(isset($_GET['cari'])){
                  $cari = $_GET['cari'];
                  echo "<i style='margin:20px 20px'>Hasil pencarian : ".$cari."</i>";
                }
              ?>
            </div>
        </div>
        
      </form>
    </div>
    <div class="card-custom">
      <div class="row">
        <?php
          include('login/db_connect.php');
        	if(isset($_GET['cari'])){
            $cari = $_GET['cari'];
            $sql="SELECT * FROM buku where judul like '%".$cari."%'";
            $query = mysqli_query($connect,$sql);			
          }else{
            $sql="SELECT * FROM buku";
            $query = mysqli_query($connect,$sql);	
          }
          while($row = mysqli_fetch_array($query)) {
            $buku_id = $row['buku_id'];
            $judul = $row['judul'];
            $pengarang = $row['pengarang'];
            $tahun = $row['tahun'];
            $detail = $row['detail'];
            $cover = $row['cover'];
            $links = $row['links'];
            $string = substr($detail, 0, 100);
          ?>
          <div class="item">
            <div class="cover">
              <img src="login/files-photo/<?php echo $cover?>" alt="<?php echo $cover?>">
            </div>
            <div class="content">
              <a class="title"><?php echo $judul;?></a>
              <a class="username">by <?php echo $pengarang;?></a>
              <div class="description">
                  <?php 
                    // echo $detail;
                    echo $string;
                  ?>...
              </div>
              <a href="#"  data-toggle="modal" data-target="#exampleModal-<?php echo $buku_id?>" class="button">Read more</a>
            </div>
          </div>
          <!-- Modal -->
          <div class="modal fade" id="exampleModal-<?php echo $buku_id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"><?php echo $judul?></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-6">
                      <img src="login/files-photo/<?php echo $cover?>" alt="The Mighty Morg by knotanumber">
                    </div>
                    <div class="col-md-6" style="margin-top:20px;">
                      <a class="title-modal"><?php echo $judul;?></a>
                      <a class="username-modal">by <?php echo $pengarang;?></a>
                      <div class="description-modal">
                          <?php 
                            echo $detail;
                          ?>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <a href="#" data-dismiss="modal"  class="button-close">Close</a>
                  <a href="<?php echo $links ?>" class="button">Read More</a>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>



<?php include('footer.php')?>