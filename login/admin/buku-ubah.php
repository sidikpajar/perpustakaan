<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-6 shadow-lg">
            <div class="box box-widget widget-user">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Ubah Buku</h3>
                  </div>
                  <form role="form" method="POST" action="buku-ubah.php" enctype="multipart/form-data">
                  <?php
                    $buku_id = $_GET['buku_id'];
                    $buku = mysqli_query($connect,"SELECT * FROM buku where buku_id='$buku_id' ");
                      while($row = mysqli_fetch_array($buku)) {
                  ?>
                      <div class="box-body">
                        <div class="form-group">
                          <label for="exampleInputEmail1">Judul Buku</label>
                          <input type="text" class="form-control" id="judul" name="judul" value="<?php echo $row['judul'] ?>" required>
                          <input type="hidden" class="form-control" id="buku_id" name="buku_id" value="<?php echo $row['buku_id']?>" required>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Pengarang</label>
                          <input type="text" class="form-control" id="pengarang" value="<?php echo $row['pengarang'] ?>" name="pengarang" required>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Tahun</label>
                          <input type="number" class="form-control" id="tahun" value="<?php echo $row['tahun'] ?>" name="tahun" required>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Links</label>
                          <input type="text" class="form-control" id="links" value="<?php echo $row['links'] ?>" name="links" required>
                        </div>
                        <label for="exampleInputEmail1">Detail</label>
                        <textarea class="form-control" id="detail" name="detail"><?php echo $row['detail'] ?></textarea>
                        <br/>
                        <div class="form-group">
                          <img height="100" src="../files-photo/<?php echo $row['cover']?>"/><br/>
                          <input type="hidden" class="form-control" id="cover_before" name="cover_before" value="<?php echo $row['cover']?>">
                          <label for="exampleInputEmail1"> Upload Cover : </label> <i><b style="color:red">*</b>jpg/png</i>
                         
                          <input class="btn btn-primary" type="file" name="cover">
                          <i>Recommendation size: 272px x 426px</i>
                        </div>
                      </div>
                    <?php } ?>
                    <div class="box-footer">
                      <a href="index.php" class="btn btn-primary">Kembali</a>
                      <button type="submit" name="submit" class="btn btn-success">Submit</button>
                    </div>
                    <?php

                      if(isset($_POST["submit"])){
                        $check = $_FILES["cover"]["tmp_name"];
                        $buku_id        = $_POST['buku_id'];
                        $lokasi_file    = $_FILES['cover']['tmp_name'];
                        $nama_file      = $_FILES['cover']['name'];
                        $folder         = "../files-photo/$buku_id+$nama_file";
                        $judul          = $_POST['judul'];
                        $pengarang      = $_POST['pengarang'];
                        $tahun          = $_POST['tahun'];
                        $detail         = $_POST['detail'];
                        $links          = $_POST['links'];
                        $cover_before   = $_POST['cover_before'];
                        if($check !== false){
                            if (move_uploaded_file($lokasi_file,"$folder")) {
                              echo "Nama File : <b>$nama_file</b> sukses di upload";
                              $sql = "UPDATE buku SET
                                buku_id='$buku_id',
                                judul='$judul',
                                pengarang='$pengarang', 
                                tahun='$tahun',
                                detail='$detail',
                                cover='$buku_id+$nama_file',
                                links='$links'
                                WHERE buku_id = '$buku_id' ";
                              if ($connect-> query($sql) === TRUE) {
                                  echo "
                                  <script type= 'text/javascript'>
                                      alert('Buku ".$judul." Berhasil diubah');
                                      window.location = 'index.php';
                                  </script>";
                                  } else {
                                      echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                                  }
                            }else{
                              $sql = "UPDATE buku SET
                              buku_id='$buku_id',
                              judul='$judul',
                              pengarang='$pengarang', 
                              tahun='$tahun',
                              detail='$detail',
                              cover='$cover_before',
                              links='$links'
                              WHERE buku_id = '$buku_id' ";
                                if ($connect-> query($sql) === TRUE) {
                                  echo "
                                  <script type= 'text/javascript'>
                                      alert('Buku ".$judul." Berhasil diubah');
                                      window.location = 'index.php';
                                  </script>";
                                  } else {
                                      echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                                  }
                            }
                          }
                          else {
                            $sql = "UPDATE buku SET
                            buku_id='$buku_id',
                            judul='$judul',
                            pengarang='$pengarang', 
                            tahun='$tahun',
                            detail='$detail',
                            cover='$cover_before',
                            links='$links'
                            WHERE buku_id = '$buku_id' ";
                              if ($connect-> query($sql) === TRUE) {
                                echo "
                                <script type= 'text/javascript'>
                                    alert('Buku ".$judul." Berhasil diubah');
                                    window.location = 'index.php';
                                </script>";
                                } else {
                                    echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                                }
                          }
                        }else{
                            echo "Please select an jpg/png file to upload.";
                      }
                    ?>
                  </form>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  
  <?php
  include("component/footer.php");
   ?>
