<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-6 shadow-lg">
            <div class="box box-widget widget-user">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Tambah Buku</h3>
                  </div>
                  <form role="form" method="POST" action="buku-tambah.php" enctype="multipart/form-data">
                    <div class="box-body">
                      
                      <div class="form-group">
                        <label for="exampleInputEmail1">Judul Buku</label>
                        <input type="text" class="form-control" id="judul" name="judul" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Pengarang</label>
                        <input type="text" class="form-control" id="pengarang" name="pengarang" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Tahun</label>
                        <input type="number" class="form-control" id="tahun" name="tahun" required>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Links</label>
                        <input type="text" class="form-control" id="links" name="links" required>
                      </div>
                      <label for="exampleInputEmail1">Detail</label>
                      <textarea class="form-control" id="detail" name="detail"></textarea>
                      <br/>
                      <div class="form-group">
                        <label for="exampleInputEmail1"> Upload Cover : </label> <i><b style="color:red">*</b>jpg/png</i>
                        <input class="btn btn-primary" type="file" name="cover" required>
                        <i>Recommendation size: 272px x 426px</i>
                      </div>

                    </div>
                    <div class="box-footer">
                      <a href="index.php" class="btn btn-primary">Kembali</a>
                      <button type="submit" name="submit" class="btn btn-success">Submit</button>
                    </div>
                    <?php

                      if(isset($_POST["submit"])){
                        $check = $_FILES["cover"]["tmp_name"];
                        if($check !== false){
                            $buku_id        = rand(100000000,999999999);
                            $lokasi_file    = $_FILES['cover']['tmp_name'];
                            $nama_file      = $_FILES['cover']['name'];
                            $folder         = "../files-photo/$buku_id+$nama_file";
                            $judul          = $_POST['judul'];
                            $pengarang      = $_POST['pengarang'];
                            $tahun          = $_POST['tahun'];
                            $detail         = $_POST['detail'];
                            $links          = $_POST['links'];
                            if (move_uploaded_file($lokasi_file,"$folder")){
                              echo "Nama File : <b>$nama_file</b> sukses di upload";
                              $sql = "INSERT INTO buku (buku_id, judul, pengarang, tahun, detail, cover, links) 
                                      VALUES ('$buku_id','$judul','$pengarang','$tahun','$detail','$buku_id+$nama_file','$links')";
                              if ($connect-> query($sql) === TRUE) {
                                  echo "
                                  <script type= 'text/javascript'>
                                      alert('Buku ".$judul." Berhasil ditambah');
                                      window.location = 'index.php';
                                  </script>";
                                  } else {
                                      echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                                    }
                              }else{
                                  echo "<script type= 'text/javascript'>alert('File upload failed, please try again');</script>";
                              }
                            }
                            else{
                              echo "File gagal di upload";
                            }
                        }else{
                            echo "Please select an jpg/png file to upload.";
                      }
                    ?>
                  </form>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  
  <?php
  include("component/footer.php");
   ?>
