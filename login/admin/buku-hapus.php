<?php
  include("component/header.php");
  include("component/sidebar.php");

if($_GET['buku_id']) {
  $buku_id = $_GET['buku_id'];

  // ambil id buku lalu hapus file cover sebelumnya
  $sql_buku="SELECT * FROM buku WHERE buku_id={$buku_id}";
  $query_buku = mysqli_query($connect,$sql_buku);
  while($row = mysqli_fetch_array($query_buku)) {
    $cover = $row['cover'];
    $files    = glob("../files-photo/{$cover}");
    foreach ($files as $file) {
      if (is_file($file))
      unlink($file); // hapus file
    }
  }

  // hapus data di database
  $sql = "DELETE FROM buku WHERE buku_id={$buku_id}";
  if ($connect-> query($sql) === TRUE ) {
    echo "
    <script type= 'text/javascript'>
        alert('Hapus buku berhasil');
        window.location = 'index.php';
    </script>";
    } else {
    echo "<script type= 'txt/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
    }
  $connect->close();
  }
 ?>
