<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <?php 
        $user_id = $_SESSION['user_id'];
        $personalSidebar="SELECT * FROM users WHERE user_id='$user_id' ";
        $querySidebar = mysqli_query( $connect, $personalSidebar );
        while($row = mysqli_fetch_array( $querySidebar )) {
      ?>
      <div class="pull-left image">
        <img src="../files-photo/users.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $row['user_name'];?> </p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
        <?php } ?>
    </div>

    <ul class="sidebar-menu" data-widget="tree">
      <li class="header" style="background-color:#DCDCDC">Menu Utama</li>
      <li><a href="index.php"><i class="fa fa-dashboard"></i> <span>List Buku</span></a></li>

      <li class="header"></li>
      <li><a href="logout.php"><i class="fa fa-sign-out text-red"></i> <span>Logout</span></a></li>
      <li><a href="change-password.php"><i class="fa fa-key text-yellow"></i> <span>Change Password</span></a></li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
