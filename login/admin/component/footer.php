<!-- /.content-wrapper -->
<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>By</b> Administrator
  </div>
</footer>

<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="../bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="../bower_components/ckeditor/ckeditor.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<script>
  $(function () {
    $('.select2').select2()
    $('.select2kontak').select2()
    $('#example0').DataTable()
    $('#example1').DataTable()
    $('#kontak-table').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })

    $('#datepicker').datepicker({
      dateFormat:'yy-mm-dd',
      autoclose: true
    })

  //  CKEDITOR.replace('editor1')
  //  CKEDITOR.replace('editor2')
  //  $('.textarea').wysihtml5()

  var editor = new FroalaEditor('#example')
  })

</script>


</body>
</html>
