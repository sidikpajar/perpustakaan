

<?php

include('../db_connect.php');
//get records from database
$query = $connect->query("SELECT * FROM buku");

if($query->num_rows > 0){
    $delimiter = ",";
    $filename = "buku_" . date('Y-m-d') . ".csv";
    
    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('buku_id', 'judul', 'pengarang', 'tahun', 'detail', 'cover', 'links');
    fputcsv($f, $fields, $delimiter);
    
    //output each row of the data, format line as csv and write to file pointer
    while($row = $query->fetch_assoc()){
        $lineData = array($row['buku_id'], $row['judul'], $row['pengarang'], $row['tahun'], $row['detail'], $row['cover'], $row['links']);
        fputcsv($f, $lineData, $delimiter);
    }
    
    //move back to beginning of file
    fseek($f, 0);
    
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
}
exit;
?>