<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        List Buku
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i>List Buku</a></li>
      </ol>
    </section>
    <section class="content-header">
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <div style="padding-top:20px">
                <a class="btn btn-primary" href="buku-tambah.php" >Tambah Buku</a>
                <a class="btn btn-primary" href="export.php" >Export File</a>
              </div>
            </div>
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                  <div class="row">
                    <div class="col-sm-12 table-responsive">
                      <iframe id="txtArea1" style="display:none"></iframe>
                      <table style=""  id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row" style="background-color:#00a65a; color: white">
                          <th>Judul</th>
                          <th>Tahun</th>
                          <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                            $sql="SELECT * FROM buku";
                            $query = mysqli_query($connect,$sql);
                            while($row = mysqli_fetch_array($query)) {
                              $buku_id = $row['buku_id'];
                              $judul = $row['judul'];
                              $pengarang = $row['pengarang'];
                              $tahun = $row['tahun'];
                              $detail = $row['detail'];
                              $cover = $row['cover'];
                              $links = $row['links'];
                            ?>
                          <tr role="row" class="odd">
                            <td>
                              <div class="row">
                                <div class="col-md-12">
                                <div class="col-md-2"><img height="100" src="../files-photo/<?php echo $cover?>"/></div>
                                <div class="col-md-8">
                                  <p>
                                    <span style="font-weight: bold"><?php echo $judul ?></span>
                                    <br/>
                                    by <?php echo $pengarang ?>
                                  </p>
                                </div>
                                </div>
                                
                              </div>
                            </td>
                            <td><?php echo $tahun ?></td>
                            <td>
                              <?php
                                  echo "<a style='margin:5px; padding:5px;' href='buku-detail.php?buku_id=".$buku_id."' class='btn btn-xs btn-primary'>DETAIL</a>";
                                  echo "<a style='margin:5px; padding:5px;' href='buku-ubah.php?buku_id=".$buku_id."' class='btn btn-xs btn-warning'>UBAH</a>";
                                  echo "<a onClick='myFunction(".$buku_id.")' style='margin:5px; padding:5px;' href='#' class='btn btn-xs btn-danger'>HAPUS</a>";
                              ?>
                            </td>
                          </tr>
                          <?php
                            }
                           ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div> 
              <div class="box-footer">
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  </div>

  <script>
    function myFunction(buku_id) {
      var r = confirm(`Anda yakin ingin menghapus id buku: ${buku_id}`);
      if (r == true) {
        window.location = 'buku-hapus.php?buku_id=' + buku_id;
      } else {
        
      }
    }
  </script>

  <?php
  include("component/footer.php");
   ?>
