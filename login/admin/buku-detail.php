<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
<div class="content-wrapper">
	<section class="content-header">
	<h1>Detail buku <a href="index.php" class="btn btn-primary">Kembali</a></h1> 
	<ol class="breadcrumb">
		<li>
			<a href="index.php"><i class="fa fa-dashboard"></i> Buku</a>
		</li>
		<li class="active">Detail Buku</li>
	</ol>
	</section>
	<section class="content-header"></section>
	<section class="content">
	<div class="row" style="margin:10px">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active">
          <a href="#acara" data-toggle="tab" aria-expanded="true">Detail Buku</a>
        </li>
        
      </ul>
      <div class="tab-content">

        <div class="tab-pane active" id="acara">
          <table class="table table-bordered">
            <tbody>
              <tr>
                <th>Keterangan</th>
                <th>Isi</th>
              </tr>
                <?php
                  $buku_id = $_GET['buku_id'];
                  $sql1="SELECT * FROM buku WHERE buku_id='$buku_id' ";
                  $query = mysqli_query( $connect, $sql1 );
                  while($row = mysqli_fetch_array( $query )) {
                ?>
                  <tr>
                    <td>Buku ID</td>
                    <td>
                      <?php echo $row['buku_id']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Judul</td>
                    <td>
                      <?php echo $row['judul']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Pengarang</td>
                    <td>
                      <?php echo $row['pengarang']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Tahun</td>
                    <td>
                      <?php echo $row['tahun']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Detail</td>
                    <td>
                      <?php echo $row['detail']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Cover</td>
                    <td>
                      <img height="300" src="../files-photo/<?php echo $row['cover']?>"/>
                    </td>
                  </tr>
                  <tr>
                    <td>links</td>
                    <td>
                      <a href="<?php echo $row['links']; ?>" target="_blank"><?php echo $row['links']; ?></a>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
        </div>

        <div class="tab-pane" id="deskripsi">
          <table class="table table-bordered">
            <tbody>
              <tr>
                <th>deskripsi</th>
              </tr>
              <?php
                  $id_acara = $_GET['id_acara'];
                  $sql2="SELECT * FROM tbl_acara
                  WHERE id_acara='$id_acara' ";
                  $query2 = mysqli_query( $connect, $sql2 );
                  while($row = mysqli_fetch_array( $query2 )) {
                ?>
              <tr>
                <td>
                  <?php echo $row['deskripsi']; ?>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>


      </div>
    </div>
	</div>
	</section>
</div>
</div>

<?php
  include("component/footer.php");
?>